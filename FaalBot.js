const config = require("./config.json");

const Discord = require('discord.js');
const { RichEmbed, Attachment } = require('discord.js');
const fs = require('fs');
const fetch = require("node-fetch");

const client = new Discord.Client();

const daggPath = './DaggSub/';

// const EventEmitter = require('events');
// const emitter = new EventEmitter();
// emitter.setMaxListeners(100);

const prefix = config.PREFIX;
const token = config.TOKEN;

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    console.log(`Bot has started with: ${client.guilds.size} servers.`);
    console.log('-----------------------------------------------');
    client.user.setActivity(config.ACTIVITY);
});

client.on('guildCreate', guild =>{
    console.log(`${guild.name} has added me to their server and has ${guild.memberCount} members.`);
});

client.on('error', console.error);

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

var daggCD = new Set();

//Commands
client.on('message', msg => {
  const args = msg.content.slice(prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  //Put future contains functions here outside of the prefix check

  if(msg.content.startsWith(prefix)){

    if(msg.author.bot){
        return;
    }

    switch(command){
      case "ping":
        msg.channel.send("pong");
        break;

      case "cha":
        if(msg.author.id === config.OWNER_ID){
           const newAct = args.join(" ");
           client.user.setActivity(newAct);
         }
         break;

      case "dagg":
        if(msg.guild.id === config.DAGG_DISCORD || msg.guild.id === config.DAGG_BDO_DISCORD){
          var filesNames = fs.readdirSync(daggPath);
          filesNames = shuffle(filesNames);
          var randomDagg = Math.floor(Math.random() * Math.floor(filesNames.length));
          if(!daggCD.has(msg.guild.id)){
            var attachment = new Attachment("./DaggSub/" + filesNames[randomDagg]);
            msg.channel.send(attachment);
            daggCD.add(msg.guild.id);
            setTimeout(() => {
              daggCD.delete(msg.guild.id);
            }, 30000);
          }
        }
      //   break;

    //PSU CS & IT Discord Specific
    case "intro":
      if(msg.channel.id === config.PSU_INTRO_CHANNEL_ID){
         const introMsg = args.join(" ");
         var logMsg = new Discord.RichEmbed()
           .setAuthor(msg.author.username)
           .setThumbnail(msg.author.avatarURL)
           .setColor('#0097e6')
           .addField('This guy:', `<@${msg.author.id}>`)
           .addField("Introduction:", introMsg);

         client.channels.get(config.PSU_INTRO_ALERT_LOG_ID).send(logMsg);
         msg.delete(5000);
         var newRole = msg.guild.roles.find(role => role.name === "You Exist");
         var introRole = msg.guild.roles.find(role => role.name === "DoIntro");
         msg.member.addRole(newRole).catch(console.error);
         msg.member.removeRole(introRole).catch(console.error);
       }
      break;

  } // End of switch
 } //check prefix
}); //END OF ON MESSAGE

client.login(token);
